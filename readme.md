# Projeto Base Cypress

##### Visando maior abrangência das tecnologias utilizadas pelo mercado, a equipe de homologação desenvolveu um projeto base pensando em usuários do framework Cypress.

#### Instalação:

1. Instale o pacote projeto-base-cypress como dependência de seu projeto atual com o comando abaixo no terminal de sua preferência:
    ```
    npm install --save-dev projeto-base-cypress
    ```


2. Para verificar se o pacote foi instalado, verifique o arquivo package.json de seu projeto conforme imagem abaixo:

    ![Alt text](assets/img/package-json.png)

	    O pacote deve estar instalado como dependência de desenvolvimento

3. Para utilizar os métodos do projeto-base-cypress você pode seguir os passos abaixo:

    ![Alt text](assets/img/import.png)

	    Importar a classe que deseja Utilizar.


    ![Alt text](assets/img/use.png)
    ![Alt text](assets/img/use-2.png)

	    Demostração de como utilizar os métodos que pertencem as classes importadas.



#### Métodos:

Método                         | Descrição                                                                | Pacote                            |  
------------------------------ | ------------------------------------------------------------------------ | --------------------------------- |
encriptar                      | Realiza a encriptação de uma string para base64                          | lib/utils/Utils                   |
decriptar                      | Realiza a decriptação de uma string encriptada em base64                 | lib/utils/Utils                   |
getKey                         | Pega o valor mais recente da coluna passada como key                     | lib/integradorCenarios/Masssa     |
gerarLinhasChaveValor          | Gera uma map com chave valor dos dados de massa                          | lib/integradorCenarios/Masssa     |
calcularIntervalo              | Calcula o intervalo entre uma data e outra e retorna a diferença em dias | lib/integradorCenarios/Masssa     |
getInvervaloEmMilli            | Retorna o intervalo em millisegundos entre duas datas                    | lib/integradorCenarios/Masssa     |
filtrarPorIntervalo            | Filtra o valor por um intervalo de datas                                 | lib/integradorCenarios/Masssa     |
filtrarPorIntervaloMaisRecente | Retorna o dado mais recente do periodo informado                         | lib/integradorCenarios/Masssa     |
filtrarPorIntervaloMaisAntigo  | Retorna o dado mais antigo do periodo informado                          | lib/integradorCenarios/Masssa     |
maisRecente                    | Retorna o último registro da massa                                       | lib/integradorCenarios/Masssa     |
filtrarPorData                 | Filtra a massa pela coluna "DATA_HORA" no formato dd/mm/yyyy hh:mm:ss    | lib/integradorCenarios/Masssa     | 
getLinhas                      | Retorna as linhas armazenadas da massa                                   | lib/integradorCenarios/Masssa     |


#### Métodos:

Método                         | Descrição                                                                | Pacote                            |  
------------------------------ | ------------------------------------------------------------------------ | --------------------------------- |
encriptar                      | Realiza a encriptação de uma string para base64                          | lib/utils/Utils                   |
decriptar                      | Realiza a decriptação de uma string encriptada em base64                 | lib/utils/Utils                   |
getKey                         | Pega o valor mais recente da coluna passada como key                     | lib/integradorCenarios/Masssa     |
gerarLinhasChaveValor          | Gera uma map com chave valor dos dados de massa                          | lib/integradorCenarios/Masssa     |
calcularIntervalo              | Calcula o intervalo entre uma data e outra e retorna a diferença em dias | lib/integradorCenarios/Masssa     |
getInvervaloEmMilli            | Retorna o intervalo em millisegundos entre duas datas                    | lib/integradorCenarios/Masssa     |
filtrarPorIntervalo            | Filtra o valor por um intervalo de datas                                 | lib/integradorCenarios/Masssa     |
filtrarPorIntervaloMaisRecente | Retorna o dado mais recente do periodo informado                         | lib/integradorCenarios/Masssa     |
filtrarPorIntervaloMaisAntigo  | Retorna o dado mais antigo do periodo informado                          | lib/integradorCenarios/Masssa     |
maisRecente                    | Retorna o último registro da massa                                       | lib/integradorCenarios/Masssa     |
filtrarPorData                 | Filtra a massa pela coluna "DATA_HORA" no formato dd/mm/yyyy hh:mm:ss    | lib/integradorCenarios/Masssa     | 
getLinhas                      | Retorna as linhas armazenadas da massa                                   | lib/integradorCenarios/Masssa     |


#### Métodos:

Método                         | Descrição                                                                | Pacote                            |  
------------------------------ | ------------------------------------------------------------------------ | --------------------------------- |
encriptar                      | Realiza a encriptação de uma string para base64                          | lib/utils/Utils                   |
decriptar                      | Realiza a decriptação de uma string encriptada em base64                 | lib/utils/Utils                   |
getKey                         | Pega o valor mais recente da coluna passada como key                     | lib/integradorCenarios/Masssa     |
gerarLinhasChaveValor          | Gera uma map com chave valor dos dados de massa                          | lib/integradorCenarios/Masssa     |
calcularIntervalo              | Calcula o intervalo entre uma data e outra e retorna a diferença em dias | lib/integradorCenarios/Masssa     |
getInvervaloEmMilli            | Retorna o intervalo em millisegundos entre duas datas                    | lib/integradorCenarios/Masssa     |
filtrarPorIntervalo            | Filtra o valor por um intervalo de datas                                 | lib/integradorCenarios/Masssa     |
filtrarPorIntervaloMaisRecente | Retorna o dado mais recente do periodo informado                         | lib/integradorCenarios/Masssa     |
filtrarPorIntervaloMaisAntigo  | Retorna o dado mais antigo do periodo informado                          | lib/integradorCenarios/Masssa     |
maisRecente                    | Retorna o último registro da massa                                       | lib/integradorCenarios/Masssa     |
filtrarPorData                 | Filtra a massa pela coluna "DATA_HORA" no formato dd/mm/yyyy hh:mm:ss    | lib/integradorCenarios/Masssa     | 
getLinhas                      | Retorna as linhas armazenadas da massa                                   | lib/integradorCenarios/Masssa     |